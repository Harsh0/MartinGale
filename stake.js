function startMartin(config){
    var step = config.step;
    var multiplier = config.multiplier;
    var interval = config.interval;//1 for 1 second
    var maxinterval = config.maxinterval;// 1 for 1 second
    var maxrisk = config.maxrisk;//0.1 for 10%
    var minrisk = config.minrisk;//0.1 for 10%
    var counter = 0;
    window.start = true;    
    var getRandomBetweenTwo = function(a,b){
        return Math.floor(Math.random()*(b-a+1)+a);  
    }
    var getTotalCoinValue = function(){
        var totalvalue = $('.chosen-single')[0].innerText;
        return +totalvalue.slice(totalvalue.indexOf('-')+2,totalvalue.lastIndexOf(' '));
    }
    var clickBet = function(betCoin){
        var roll = [$('[origin="Roll < 48"]')[0],$('[origin="Roll > 52"]')[0]];
        $('input.bet')[0].value = betCoin.toFixed(8);
        let random = getRandomBetweenTwo(0,1);
        roll[random].click();
    }
    var martinGale = function(totalvalue,coinval){
        if(!coinval){
            coinval = (totalvalue*(multiplier-1))/(multiplier**step-1);
        }
        if(window.start){
            clickBet(coinval);
            checkStatus(totalvalue,coinval);       
        }
    }
    var checkStatus = function(lastValue,coinval){
        setTimeout(function(){
            var currentValue = getTotalCoinValue();
            if(currentValue!=lastValue){
                if(currentValue<lastValue){
                    if(currentValue<=((1-minrisk)*initialValue)){
                        stopMartin();
                    }else{
                        martinGale(currentValue,coinval*multiplier);
                    }
                }else{
                    if(currentValue>=((1+maxrisk)*initialValue)){
                        stopMartin();
                    }else{
                        martinGale(currentValue);
                    }
                }
            }else{
                counter++;
                if(counter>=10){
                    var color = $('.dice_table:last').children('tbody').children('tr:first').children('td:last').attr('class');
                    if(color=='red'){
                        martinGale(currentValue,coinval*multiplier);
                    }else{
                        martinGale(currentValue);
                    }
                    counter =0;
                }
            }
        },getRandomBetweenTwo(interval*1000,(maxinterval||interval)*1000));
    }
    window.stopMartin = function(){
        window.start = false;
    }
    var initialValue = getTotalCoinValue();    
    martinGale(initialValue);
}
function invokeMartin(){
    var step = +window.prompt("What is step",10);
    var multiplier = +window.prompt("What is multiplier",2.63);
    var interval = +window.prompt("What is Interval in second",1);
    var maxrisk = +window.prompt("What is max value risk, 0.2 for 20%",0.1);
    var minrisk = +window.prompt("What is min value risk, 0.2 for 20%",0.1);    
    startMartin({
        step:step,
        multiplier:multiplier,
        interval:interval,
        maxrisk:maxrisk,
        minrisk:minrisk
    })
}
var wants = window.confirm("Do you want the automate betting");
if(wants){
    console.log("invoke stopMartin() for stop");
    invokeMartin();
}

